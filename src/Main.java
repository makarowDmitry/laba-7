
import java.util.InputMismatchException;
import java.util.Scanner;


/**
 * Класс осуществляет подсчёт цифр в натуральном числе
 *
 * @author Макаров Д.А 17ИТ18
 */
public class Main {
    private static final int LIMITMAX = 1000000000;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double num = 0;
        System.out.println("Введите натуральное число");
        try {
            num = scanner.nextDouble();
            while (checking(num)) {
                num = scanner.nextDouble();
            }
        } catch (InputMismatchException e) {
            e.printStackTrace();
        }
        System.out.println(count(num));
    }

    /**
     * Возвращает true если число не прошло проверку на корректность
     *
     * @param num - проверяемое число
     * @return - true если число не прошло проверку
     * @throws - число заданно не корректно
     */
    public static boolean checking(double num) throws ArithmeticException {
        boolean check = false;
        try {
            if (num < 1 || num > LIMITMAX || num - Math.floor(num) != 0) {
                check = true;
                throw new ArithmeticException("Число заданно не корректно");
            }
        } catch (ArithmeticException e) {
            e.printStackTrace();
        }
        return check;
    }

    /**
     * Возвращает количество цифр в числе
     *
     * @param num - число
     * @return количество цифр
     */
    public static int count(double num) {
        int sum = 0;
        while (num > 1) {
            num = num / 10;
            sum++;
        }
        System.out.println("Количество символов подсчитоно в методе count: " + sum);
        return sum;
    }
}