import org.junit.Test;

import static org.junit.Assert.*;

public class MainTest {
    /**
     * Осуществляет тестирование метода checking на проверку натурального числа на отрицательность
     */
    @Test
    public void negativeNum() {
        int num = -1 + (int) (Math.random() * -100);
        assertEquals(true, Main.checking(num));
    }

    /**
     * Осуществляет тестирование метода checking на провекрку натурального числа на положительность
     */
    @Test
    public void positivNum() {
        int num = 1 + (int) (Math.random() * 100);
        assertEquals(false, Main.checking(num));
    }

    /**
     * Осуществляет тестирование метода checking на проверку натурального числа на целостность
     */
    @Test
    public void fractionNum() {
        double num = 2.5;
        assertEquals(true, Main.checking(num));
    }

    /**
     * Осуществляет тестирование метода checking на проверку лимита разрешённого натурального числа
     */
    @Test
    public void limitNum() {
        double num = 1000000001;
        assertEquals(true, Main.checking(num));
    }

    /**
     * Осуществляет тестирование метода count на подсчёт цифр в натуральном числе
     */
    @Test
    public void countNum() {
        double num = 59343;
        int result = 5;
        assertEquals(result, Main.count(num));
    }

    /**
     * Осуществляет тестирование метода checking проверку натурального числа на нуль
     */
    @Test
    public void ZeroNum() {
        double num = 0;
        assertEquals(true, Main.checking(num));
    }

}